import { DestinoViaje } from './destino-viaje.model';
import { Action } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { ListaDestinosComponent } from '../components/lista-destinos/lista-destinos.component';

//Estado
export class DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestinosViajesState() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

// Acciones
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos viajes] Vote up',
    VOTE_DOWN = '[Destinos viajes] Vote down',
    INIT_MY_DATA = '[Destino viajes] Init my data'
}

export class NuevoDestinoAction implements Action{
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor (public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {};
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {};
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {};
}

export class InitMyDataAction implements Action {
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor (public destinos: string[]) {}
}

export type DestinoViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction;

// Reducers
export function reduceDestinoViajes(
    state: DestinosViajesState,
    action: DestinoViajesActions
): DestinosViajesState {
    switch(action.type){
        case DestinosViajesActionTypes.NUEVO_DESTINO:
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO:
            let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            return {
                ...state,
                favorito: fav
            }
        case DestinosViajesActionTypes.VOTE_UP:
            const u: DestinoViaje = (action as VoteUpAction).destino;
            u.voteUp();
            return {...state};
        case DestinosViajesActionTypes.VOTE_DOWN:
            const d: DestinoViaje = (action as VoteDownAction).destino;
            d.voteDown();
            return {...state};
        case DestinosViajesActionTypes.INIT_MY_DATA:
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            }
    }
    return state;
}

// effects
@Injectable()
export class DesintoViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.action$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor (private action$: Actions) {}
}