import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { inject } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viaje-estate.model';
import { DestinoViaje } from './destino-viaje.model';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[];
    current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient
        ) {
        this.store
            .select(state => state.destinos)
            .subscribe((data) => {
                console.log('destinos sub store');
                console.log(data);
                this.destinos = data.items;
            });
        this.store
            .subscribe((data) => {
                console.log('all store');
                console.log(data);
            });
        this.destinos=[];
    }

    add(d:DestinoViaje){
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndPoint+'/my', {nuevo: d.nombre}, {headers: headers});
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200){
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDB = db;
                myDB.destinos.add(d);
                console.log('Todos los destinos de la db!');
                myDB.destinos.toArray().then(destinos => console.log(destinos));
            }
        });
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    getById(id:string): DestinoViaje[] {
        return this.destinos.filter(function(d){return d.id.toString() == id;}[0]);
    }

    elegir(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(d));
        
        /*
        let nDest: DestinoViaje[] = [];
        this.destinos.forEach(x => nDest.push(new DestinoViaje(x.nombre, x.imagenUrl, x.votes)));
        nDest.forEach(x => x.setSelected(false));
        */
       /*
        this.destinos.forEach(x => {
            let b: boolean = x.isEqual(d);
            x.setSelected(b);
            if (b) {
                this.current.next(x);
            }
        });
        */
        /*
        for (let eFav of this.destinos){
            eFav.setSelected(eFav.isEqual(d));
            if (eFav.isEqual(d)){
                this.current.next(eFav);
            }
        }
        */
    }

    subscribeOnChange(fn) {
        this.current.subscribe(fn);
    }
}