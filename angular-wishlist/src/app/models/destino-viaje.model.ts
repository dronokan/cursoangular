import { ObjectUnsubscribedError } from 'rxjs';
import { v4 as uuid } from 'uuid';

export class DestinoViaje {
	//nombre:string;
	//imagenUrl:string;
	selected: boolean;
	servicios: string[];
	//votes: number;
	id = uuid();

	constructor(public nombre:string, public imagenUrl:string, public votes:number=0){
		//this.nombre=n;
		//this.imagenUrl=u;
		this.servicios = ["Piscina", "Desayuno", "Barra libre"];
		this.selected=false;
		this.votes=0;
	}

	isSelected():boolean {
		return this.selected;
	}
	
	isEqual(eDest: DestinoViaje): boolean {
		return this.nombre==eDest.nombre && this.imagenUrl==eDest.imagenUrl && this.id==eDest.id;
	}

	setSelected(nVal: boolean){
		this.selected=nVal;
	}
	
	voteUp() {
		this.votes++;
	}

	voteDown() {
		this.votes--;
	}
}