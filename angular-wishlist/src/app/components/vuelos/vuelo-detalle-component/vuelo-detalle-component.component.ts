import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-vuelo-detalle-component',
  templateUrl: './vuelo-detalle-component.component.html',
  styleUrls: ['./vuelo-detalle-component.component.css']
})
export class VueloDetalleComponent implements OnInit {
  id: any;

  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params => { this.id = params['id']; });
  }

  ngOnInit(): void {
  }

}
