import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VueloDetalleComponent } from './vuelo-detalle-component.component';

describe('VueloDetalleComponent', () => {
  let component: VueloDetalleComponent;
  let fixture: ComponentFixture<VueloDetalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VueloDetalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VueloDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
