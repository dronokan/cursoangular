import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { inject } from '@angular/core/testing';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-formulario-destino-viaje',
  templateUrl: './formulario-destino-viaje.component.html',
  styleUrls: ['./formulario-destino-viaje.component.css']
})
export class FormularioDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLong: number;
  sugerencias: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.minLong=7;
    this.onItemAdded = new EventEmitter<DestinoViaje>();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.validParam(this.minLong)
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      //console.log("cambio en formulario: " + form);
    });
  }

  guardar(n:string, u:string): boolean {
    const d = new DestinoViaje(n, u);
    this.onItemAdded.emit(d);
    return false;
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text:string) => ajax(this.config.apiEndPoint+'/ciudades?q='+text))
      ).subscribe(AjaxResponse => {
        this.sugerencias = AjaxResponse.response;
      });
  }

  validLong(control: FormControl): { [s: string]: boolean; } {
    const l = control.value.toString().length;
    if (l < 5){
      return { validLongitud: true };
    }
    return null;
  }

  validParam(mLong: number): ValidatorFn {
    return (control: FormControl): { [s:string]:boolean } | null => {
      const l = control.value.toString().length;
      if (l<mLong){
        return { valMinLong: true };
      }
      return null;
    }
  }

}
