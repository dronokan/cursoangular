import { applySourceSpanToExpressionIfNeeded } from '@angular/compiler/src/output/output_ast';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { concat } from 'rxjs';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino-viaje-estate.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
	@Output() onItemAdded: EventEmitter<DestinoViaje>;
	updates: string[] = [];
	destinosApiClient: DestinosApiClient = new DestinosApiClient(this.store);

	//destinos: DestinoViaje[];
	constructor(private store: Store<AppState>) {
		//this.destinos = [];
		this.onItemAdded = new EventEmitter();
		store.select(state => state.destinos.favorito)
			.subscribe(d => {
				this.destinosApiClient.elegir(d);
				if (d!=null){
					this.updates.push('Se ha elegido ' + d.nombre);
				}
			});
		this.destinosApiClient.subscribeOnChange((d:DestinoViaje) => {
			
		});
	}

	allDestinos() {
		return this.destinosApiClient.getAll();
	}

	ngOnInit(): void {
	}

	agregado(d: DestinoViaje) {
		this.destinosApiClient.add(d);
		this.onItemAdded.emit(d);
		this.store.dispatch(new NuevoDestinoAction(d));
	}

	elegido(d: DestinoViaje){
		this.destinosApiClient.elegir(d);
		this.store.dispatch(new ElegidoFavoritoAction(d));
		//this.destinos.forEach(function (x) { x.setSelected(false) });
		//d.setSelected(true);
	}
}