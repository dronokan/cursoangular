import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects'

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormularioDestinoViajeComponent } from './components/formulario-destino-viaje/formulario-destino-viaje.component';
import { 
  DesintoViajesEffects,
  DestinosViajesActionTypes,
  DestinosViajesState, 
  initializeDestinosViajesState, 
  InitMyDataAction, 
  reduceDestinoViajes 
} from './models/destino-viaje-estate.model';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosMainComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VueloDetalleComponent } from './components/vuelos/vuelo-detalle-component/vuelo-detalle-component.component';
import { VuelosComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { ReservasModule } from './reservas/reservas.module'
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http'
import { Dexie } from 'dexie'
import { DestinoViaje } from './models/destino-viaje.model';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

export const childreRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent },
  { path: ':id', component: VueloDetalleComponent }
];

const rutas: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,  
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childreRoutesVuelos
  }
];

//appinit
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinoViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) {  }
  async initializeDestinoViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-securidad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint+'/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
//fin appInit

//app config
export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localHost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//app fin config

// redux init
export class AppState {
  destinos: DestinosViajesState;
}

let reducers: ActionReducerMap<AppState> = {
  destinos: reduceDestinoViajes
}

let reducersInitialState =  {
  destinos: initializeDestinosViajesState()
}
// redux fin init

export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

//dexie init
@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl', //esto quiere decir que se genera una primera versión de la tabla 'destinos' y que va a requerid un id, nombre y imagenURL, que son los componentes de DestinoViaje
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase;
//fin dexie init

//i18n init
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient){}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
      .where('lang')
      .equals(lang)
      .toArray()
      .then(results => {
        if (results.length===0){
          return this.http
            .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint+'/api/translation?lang='+lang)
            .toPromise()
            .then(apiResults => {
              db.translations.bulkAdd(apiResults);
              return apiResults;
            });
        }
        return results;
      }).then((traducciones) => {
        console.log('traducciones cargadas');
        console.log(traducciones);
        return traducciones;
      }).then((traducciones) => {
        return traducciones.map((t) => ({ [t.key]: t.value }));
      });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
//fin i18n

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormularioDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VueloDetalleComponent,
    VuelosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    RouterModule.forRoot(rutas),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([DesintoViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslationLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    //DestinosApiClient,
    AuthService,
    UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
