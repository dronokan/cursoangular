import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(userName:string, passWord:string):boolean {
    if (userName==="user" && passWord==="password"){
      localStorage.setItem('username', userName);
      return true;
    }
    return false;
  }

  logout():any {
    localStorage.removeItem('username');
  }

  getUser():any{
    return localStorage.getItem('username');
  }

  isLoggedIn():boolean {
    return this.getUser() !== null;
  }
}
